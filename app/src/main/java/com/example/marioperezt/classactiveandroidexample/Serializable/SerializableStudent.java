package com.example.marioperezt.classactiveandroidexample.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marioperezt on 11/7/17.
 */

public class SerializableStudent {

    @SerializedName("id")
    private int mId;

    @SerializedName("Name")
    private String mName;

    public SerializableStudent(){

    }

    public SerializableStudent(int id, String name){
        mId = id;
        mName = name;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }
}
