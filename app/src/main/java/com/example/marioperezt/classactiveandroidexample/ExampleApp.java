package com.example.marioperezt.classactiveandroidexample;

import com.activeandroid.ActiveAndroid;
/**
 * Created by marioperezt on 11/7/17.
 */
public class ExampleApp extends com.activeandroid.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);

    }
}
