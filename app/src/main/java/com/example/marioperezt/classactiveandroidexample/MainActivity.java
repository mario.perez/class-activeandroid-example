package com.example.marioperezt.classactiveandroidexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.marioperezt.classactiveandroidexample.DbConection.Entities;
import com.example.marioperezt.classactiveandroidexample.DbModels.Student;
import com.example.marioperezt.classactiveandroidexample.Serializable.SerializableStudent;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText mEtId;
    private EditText mEtName;
    private EditText mEtLastName;
    private TextView mTvResult;
    private Button mBtnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEtId = (EditText) findViewById(R.id.et_id);
        mEtName = (EditText) findViewById(R.id.et_name);
        mEtLastName = (EditText) findViewById(R.id.et_last_name);
        mTvResult = (TextView) findViewById(R.id.tv_result);
        mBtnSave = (Button) findViewById(R.id.bt_guardar);
        mBtnSave.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {


        Entities.saveStudent(new Student(Integer.parseInt(mEtId.getText().toString()),
                mEtName.getText().toString(),mEtLastName.getText().toString()));

        List<Student> result = Entities.getAllStudents();
        List<SerializableStudent> resultList =  new ArrayList<>();
        for(Student student: result){
            resultList.add(new SerializableStudent(student.mIdCatalog,
                    student.mName+" " + student.mLastName));
        }
        mTvResult.setText(new GSonUtils<List<SerializableStudent>>().serializeToJson(resultList));


    }
}
