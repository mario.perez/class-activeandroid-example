package com.example.marioperezt.classactiveandroidexample.DbConection;

import android.app.Activity;

import com.activeandroid.query.Select;
import com.example.marioperezt.classactiveandroidexample.DbModels.Student;

import java.util.List;

/**
 * Created by marioperezt on 11/7/17.
 */
//TODO: Crear una clase o metodos que guarden en la BD.
public class Entities extends Activity {

    public static boolean saveStudent(Student student){
        long id;
        id=student.save();
        return id!=0;
    }

    public static List<Student> getAllStudents(){
        return new Select().from(Student.class).execute()  ;
    }

}
