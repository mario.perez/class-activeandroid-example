package com.example.marioperezt.classactiveandroidexample.DbModels;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by marioperezt on 11/7/17.
 */
//TODO: Crear un modelo para crear las tablas.
@Table(name="Student")
public class Student extends Model {

    @Column(name = "IdStudent")
    public int mIdCatalog;

    @Column(name = "Name")
    public String mName;

    @Column(name = "LastName")
    public String mLastName;

    public Student(){
        super();
    }

    public Student(int id, String name, String lastName){
        mIdCatalog = id;
        mName = name;
        mLastName = lastName;
    }

}
